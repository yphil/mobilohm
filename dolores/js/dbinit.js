/*
 * Dolores App
 * Copyright (C) 2023 yPhil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

let request = window.indexedDB.open("WorkoutDB", 1);

request.onerror = function(event) {
  console.log("Error opening DB", event);
};

request.onupgradeneeded = function(event) {
  window.db = event.target.result;

  if (!window.db.objectStoreNames.contains('workouts')) {
    let objectStore = window.db.createObjectStore("workouts", { keyPath: "date" });
    objectStore.createIndex("date", "date", { unique: false });
  }

  if (!window.db.objectStoreNames.contains('values')) {
    let valuesStore = window.db.createObjectStore("values");
  }

  if (!window.db.objectStoreNames.contains('settings')) {
    window.db.createObjectStore("settings");
  }

  console.log("Success upgrading DB");
  document.dispatchEvent(new Event('dbReady')); // Move this line here
  console.log('dbReady sent');
};

request.onsuccess = function(event) {
  window.db = event.target.result;
  document.dispatchEvent(new Event('dbReady')); // Move this line here
  console.log('%cDB Init OK', 'color: green;');
};
