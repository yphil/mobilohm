/*
 * Dolores App
 * Copyright (C) 2023 yPhil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Get the current date
let currentMonth = new Date().getMonth();
let currentYear = new Date().getFullYear();
let currentDay = new Date().getDate();


let prevMonthElement = document.getElementById('prev-month');
if (prevMonthElement) {
    prevMonthElement.addEventListener('click', async () => {
        // If it's not January, decrement the month. Otherwise, set the month to December and decrement the year.
        if (currentMonth > 0) {
            currentMonth--;
        } else {
            currentMonth = 11;
            currentYear--;
        }
        // Update the calendar and display the day stats
        await updateCalendar(currentMonth, currentYear);
        displayDayStats();

    });
}

let nextMonthElement = document.getElementById('next-month');
if (nextMonthElement) {
    nextMonthElement.addEventListener('click', async () => {
        if (currentMonth < 11) {
            currentMonth++;
        } else {
            currentMonth = 0;
            currentYear++;
        }
        // Update the calendar and display the day stats
        await updateCalendar(currentMonth, currentYear);
        displayDayStats();
    });
}

let todayMonthElement = document.getElementById('today-month');
if (todayMonthElement) {
    todayMonthElement.addEventListener('click', async () => {
        currentDate = new Date();
        currentDay = currentDate.getDate();
        currentMonth = currentDate.getMonth();
        currentYear = currentDate.getFullYear();
        await updateCalendar(currentMonth, currentYear, 'month');

    });
}

let prevWeekElement = document.getElementById('prev-week');
if (prevWeekElement) {
    prevWeekElement.addEventListener('click', async () => {
        currentDate.setDate(currentDate.getDate() - 7);
        currentDay = currentDate.getDate();
        currentMonth = currentDate.getMonth();
        currentYear = currentDate.getFullYear();
        await updateCalendar(currentMonth, currentYear, 'week');
    });
}

let nextWeekElement = document.getElementById('next-week');
if (nextWeekElement) {
    nextWeekElement.addEventListener('click', async () => {
        currentDate.setDate(currentDate.getDate() - currentDate.getDay() + 7);
        currentDay = currentDate.getDate();
        currentMonth = currentDate.getMonth();
        currentYear = currentDate.getFullYear();
        await updateCalendar(currentMonth, currentYear, 'week');
    });
}

let todayWeekElement = document.getElementById('today-week');
if (todayWeekElement) {
    todayWeekElement.addEventListener('click', async () => {
        currentDate = new Date();
        currentDay = currentDate.getDate();
        currentMonth = currentDate.getMonth();
        currentYear = currentDate.getFullYear();
        await updateCalendar(currentMonth, currentYear, 'week');
    });
}

let scaleMax = 10; // Maximum value of the scale

async function checkWorkoutDay(date) {
    return new Promise((resolve, reject) => {
        let openRequest = indexedDB.open("WorkoutDB", 1);

        openRequest.onsuccess = function() {
            let db = openRequest.result;
            let transaction = db.transaction("workouts");
            let workouts = transaction.objectStore("workouts");
            let request = workouts.getAll();

            request.onsuccess = function() {
                let workoutDates = request.result.map(workout => workout.date);
                resolve(workoutDates.includes(date));
            };

            request.onerror = function() {
                reject(request.error);
            };
        };

        openRequest.onerror = function() {
            reject(openRequest.error);
        };
    });
}

async function getTotalWorkoutTime(date) {
    return new Promise((resolve, reject) => {
        let openRequest = indexedDB.open("WorkoutDB", 1);

        openRequest.onsuccess = function() {
            let db = openRequest.result;
            let transaction = db.transaction("workouts");
            let workouts = transaction.objectStore("workouts");
            let index = workouts.index("date");
            let request = index.getAll(date);

            request.onsuccess = function() {
                let totalWorkoutTime = request.result.reduce((total, workout) => total + workout.workoutTime, 0);
                resolve(totalWorkoutTime);
            };

            request.onerror = function() {
                reject(request.error);
            };
        };

        openRequest.onerror = function() {
            reject(openRequest.error);
        };
    });
}

function getWeekNumber(d) {
    d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
    d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay() || 7));
    let yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
    let weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7);
    return weekNo;
}

let currentTable; // Variable to store the reference to the current table

function getBottomRowContent(event) {
    const clickedTd = event.target.closest('td');
    const table = currentTable;
    const bottomRow = table.rows[table.rows.length - 1];
    let displayedDate;
    const columnIndex = Array.from(clickedTd.parentNode.children).indexOf(clickedTd);
    const bottomRowTd = bottomRow.cells[columnIndex];

    if (clickedTd.classList.contains('day-number-td')) {
        displayedDate = clickedTd.dataset.date;
    } else {
        displayedDate = clickedTd.dataset.date.substring(0, 8) + bottomRowTd.textContent;
    }

    console.log('clickedTd.dataset: ', clickedTd.dataset);

    openModal(displayedDate, clickedTd.dataset.seconds);
}

// Asynchronous function to update the calendar
async function updateCalendar(month, year, mode = 'month') {
    const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    let calendarBodyId = mode === 'week' ? 'week-calendar' : 'calendar';
    let calendarBody = document.getElementById(calendarBodyId).querySelector('.calendar-body');

    document.getElementById('month-year').textContent = monthNames[month] + ' ' + year;

    const daysInMonth = mode === 'week' ? 7 : 32 - new Date(year, month, 32).getDate();

    if (mode === 'week') {
        let firstDayOfWeek = new Date(year, month, currentDay);
        firstDayOfWeek.setDate(firstDayOfWeek.getDate() - firstDayOfWeek.getDay());
        let weekNumber = getWeekNumber(firstDayOfWeek);
        document.getElementById('week-year').textContent = "Week " + weekNumber;
    }

    let dayPromises = [];
    let workoutTimePromises = [];

    let firstDayOfWeek = mode === 'week' ? currentDate.getDate() - currentDate.getDay() : 1;

    for (let day = 0; day < daysInMonth; day++) {
        let workoutDate = year + '-' + (month + 1).toString().padStart(2, '0') + '-' + (firstDayOfWeek + day).toString().padStart(2, '0');
        dayPromises.push(checkWorkoutDay(workoutDate));
        workoutTimePromises.push(getTotalWorkoutTime(workoutDate));
    }

    let isWorkoutDayResults = await Promise.all(dayPromises);
    let workoutTimeResults = await Promise.all(workoutTimePromises);

    let maxIntervals = Math.max(...workoutTimeResults.map(time => Math.ceil(time / 300)));

    let rowHeight = (100 / maxIntervals).toFixed(2);

    let headerRow = '<thead class="total-row"><td></td>';

    let totalWorkoutTimeForWeekInSeconds = 0;
    let totalWorkoutTimeForMonthInSeconds = 0;

    for (let day = 1; day <= daysInMonth; day++) {

        // Add workout time to total for the month
        totalWorkoutTimeForMonthInSeconds += workoutTimeResults[day - 1];

        // Add workout time to total for the week only if the mode is 'week' and it's one of the first 7 days
        if (mode === 'week' && day <= 7) {
            totalWorkoutTimeForWeekInSeconds += workoutTimeResults[day - 1];
        }
    }

    if (mode === 'week') {
        document.getElementById('week-year').textContent = document.getElementById('week-year').textContent + ': ' + secondsToMinutes(totalWorkoutTimeForWeekInSeconds);
    } else if (mode === 'month') {
        document.getElementById('month-year').textContent = document.getElementById('month-year').textContent + ': ' + secondsToMinutes(totalWorkoutTimeForMonthInSeconds);
    }

    headerRow += '</thead>';

    let scaleTable = '<table class="table is-fullwidth' + ' calendar-' + mode + '" id="calendar-table">' + headerRow + '<tbody>';

    for (let i = maxIntervals; i > 0; i--) {
        let rowClass = i === 1 ? ' class="day-values-row"' : '';
        scaleTable += '<tr' + rowClass + ' style="height:' + rowHeight + '%;"><td><span>' + (i * 5) + '</span></td>';

        for (let day = 1; day <= daysInMonth; day++) {
            let paddedDay = String(day).padStart(2, '0');
            let paddedMonth = String(month + 1).padStart(2, '0');
            var todaysDate = year + '-' + paddedMonth + '-' + paddedDay;
            let cellsToColor = Math.ceil(workoutTimeResults[day - 1] / 300);
            let shouldColorCell = i <= cellsToColor;
            let tdAttributes = 'onclick="getBottomRowContent(event)" data-date="' + todaysDate + '" data-seconds="' + workoutTimeResults[day - 1] + '"';
            let cellClass = shouldColorCell ? 'stats-table-td workout-day has-background-link' : 'stats-table-td';
            scaleTable += '<td ' + tdAttributes + ' class="' + cellClass + '"></td>';
        }

        scaleTable += '</tr>';
    }

    if (maxIntervals === 0) {
        scaleTable += '<tr class="empty" style="height:100%;"><td class"empty-first-cell"></td>';
        for (let day = 1; day <= daysInMonth; day++) {
            let paddedDay = String(day).padStart(2, '0');
            let paddedMonth = String(month + 1).padStart(2, '0');
            var todaysDate = year + '-' + paddedMonth + '-' + paddedDay;
            scaleTable += '<td onclick="getBottomRowContent(event)" data-date="' + todaysDate + '" data-seconds="0" class="empty-calendar-td"></td>';
        }
        scaleTable += '</tr>';
    }


    scaleTable += '<tfoot class="day-numbers-row"><td class"empty-first-cell"></td>';

    for (let day = firstDayOfWeek; day < firstDayOfWeek + daysInMonth; day++) {
        scaleTable += '<td class="day-number-td day-number-' + mode + '-td"><span class="day-number">' + day + '</span></td>';
    }

    scaleTable += '</tfoot>';

    scaleTable += '</tbody></table>';

    calendarBody.innerHTML = scaleTable;

    let statsTableTds = document.querySelectorAll('.stats-table-td');
    let statsTableWorkoutDays = document.querySelectorAll('.workout-day');
    let statsTableWorkoutDayTd = document.querySelector('.workout-day');

    if (statsTableWorkoutDayTd && statsTableTds.length && statsTableWorkoutDays.length) {
        const computedStyle = getComputedStyle(statsTableWorkoutDayTd);
        const backgroundColor = computedStyle.backgroundColor;
        statsTableTds.forEach((workoutDay, index) => {
            if (computedStyle) {
                statsTableTds[index].style.borderTop = `1px solid ${backgroundColor}`;
                statsTableTds[index].style.borderBottom = `1px solid ${backgroundColor}`;
            }
        });
    }

    // Store the reference to the current table before updating its content
    currentTable = document.querySelector('.calendar-' + mode);

}

let monthTabElement = document.getElementById('month-tab');
if (monthTabElement) {
    monthTabElement.addEventListener('click', async () => {
        await updateCalendar(currentMonth, currentYear, 'month');
    });
}

let weekTabElement = document.getElementById('week-tab');
if (weekTabElement) {
    weekTabElement.addEventListener('click', async () => {
        await updateCalendar(currentMonth, currentYear, 'week');
    });
}
