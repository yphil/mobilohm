# MobilOhm

A collection of mobile-friendly apps

No bloat, no useless features, Free, Libre and Open-Source, syncs in the cloud 😎

![MobilOhm Apps](img/mobilohm-apps.png "MobilOhm Apps")
